package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.data.DTO;
import com.example.demo.data.PaymentTransactionRepository;

@RestController
@RequestMapping("/transactions")
public class PaymentTransactionsController  {

    @Autowired
    private PaymentTransactionRepository transactionRepository;

    @GetMapping("{id}")
    public ResponseEntity<?> get(@PathVariable String id) {
		Optional<DTO> dto = transactionRepository.findById(Long.parseLong(id));
        return  dto.isPresent()?ResponseEntity.ok(dto.get().toString()):ResponseEntity.notFound().build();
    }
}
