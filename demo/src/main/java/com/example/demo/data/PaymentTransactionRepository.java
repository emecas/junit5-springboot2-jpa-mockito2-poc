package com.example.demo.data;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Component;

//@Component
public interface PaymentTransactionRepository extends CrudRepository<DTO, String>{

	public Optional<DTO> findById(long id);

}
